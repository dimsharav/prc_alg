n = int(input('Введите четное число элементов: '))
massiv = []

for i in range(n):
    print('Введите элемент № ', i)
    x = int(input())
    massiv.append(x)

print('Получился массив:\n', massiv)

for i in range(0,n,2):
    massiv[i], massiv[i+1] = massiv[i+1], massiv[i]

print('После обмена значениями получился массив:')
print(massiv)
