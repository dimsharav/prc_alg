#!/usr/bin/env python

'''
Программа для изучения таблицы умножения.
Разработана в рамках курса
"Основы алгоритмизации и программирования".
ПРК, группа КСК-11-В3
'''

from random import randint


def is_continue():
    '''
    Проверка продолжения цикла
    '''
    q = input('Нажмите "Y" для продолжения: ')
    return q in ['y', 'Y']
   
 
if __name__ == '__main__':
    print('Проверка знаний таблицы умножения\n')
    while is_continue():
        j = int(input('Введите число для изучения: '))
        score = 0
        using_list = []
        for i in range(10):

            x = randint(1, 10)
            while x in using_list:
                x = randint(1, 10)
            using_list.append(x)
            
            y = j * x
            string_question = '%d * %d = ' % (j, x)
            answer = int(input(string_question))
            if answer == y:
                print('Правильно!')
                score += 1
            else:
                print('Неправильно!')
        print('Правильных ответов: ', score)
        
    print('До свидания!')
