def exp(x,n):
    '''Возведение x в степень n'''
    y = x ** n
    return y

def square(x):
    '''Возведение х в квадрат'''
    y = exp(x,2)
    return y
    
print(square(1024))

