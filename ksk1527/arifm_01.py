#!/usr/bin/python3

print("Условие задачи:")
print("Вывести число суток в указанном числе часов (n)")
print("и указать, сколько часов осталось")

n = int(input("Введите n: "))

n_sutok = n // 24
n_chasov = n % 24

print("Число суток:", n_sutok, "Число часов:", n_chasov)
