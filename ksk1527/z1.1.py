#!/usr/bin/env python3
from math import pi

print("Вывести число Пи с извращениями или без?")
print("Если с ними, введите 1, иначе введите хоть что:")
choice = int(input())

if choice == 1:
    x = round(pi, 2)
    print(x)
else:
    print(3.14)
