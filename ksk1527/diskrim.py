#!/usr/bin/python

print('Число действительных корней кв. уравнения')

a = float(input('Введите a: '))
b = float(input('Введите b: '))
c = float(input('Введите c: '))

diskr = b**2 - 4*a*c

if diskr < 0:
    print('Нет корней')
elif diskr == 0:
    print('1 корень')
else:
    print('2 корня')
