#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from math import factorial as f

def a(n, k):
    '''
    Функция вычисляет число размещений без повторений
    из n по k.
    '''
    s = f(n)/f(n-k)
    return s

def c(n, k):
    '''
    Функция вычисляет число сочетаний без повторений
    из n по k.
    '''
    pass

def c_with_repeat(n, k):
    '''
    Функция вычисляет число сочетаний с повторениями
    из n по k.
    '''
    pass

def p(n):
    '''
    Функция вычисляет число перестановок n элементов
    '''
    pass

    
if __name__ == "__main__":
    n = int(input('Введите n: '))
    k = int(input('Введите k: '))
    print(a(n,k))

