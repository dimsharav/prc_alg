'''
Решение задачи из проверочной по дискретной математики:
В компьютерной игре есть 6 видов ручного оружия, 10 видов брони и 5 видов инструментов. Персонаж может нести одновременно 2 вида оружия, 3 инструмента и 3 вида брони. Сколько имеется наборов экипировки в игре?
'''

from defs_combinatorics import a

print(a(6,2) * a(10,3) * a(5,3))

