#!/usr/bin/env python
# -*- coding: utf8 -*-

print('Программа для шифрования строки методом сдвига (Шифр Цезаря)')
s = str(input('Введите строку для шифрования: '))
step = int(input('Введите шаг шифрования: '))

alphabet = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя'
s_encrypted = ''

for i in s:
    if i in alphabet:
        index = alphabet.find(i)
        new_index = (index + step) % len(alphabet)
        s_encrypted += alphabet[new_index]
    else:
        s_encrypted += i
    
print('Зашифрованная строка:')

print(s_encrypted)
